#Imports
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys

'''
Función que permite graficar el arreglo.
'''
def graficar(arreglo):
    x=[]
    y=[]
    labels=[]
    n = len(arreglo)

    for i in range(n):
        x.append(i+1)
        y.append(arreglo[i][1])
        labels.append(arreglo[i][0])

    # Graficando por colores y barras.
    plt.bar(x, y, tick_label = labels, width = 0.8, color = ['red', 'blue']) 
    
    # Asignando los nombres de los ejes y el titulo del grafico 
    plt.xlabel('Eje x') 
    plt.ylabel('Eje y') 
    plt.title('Grafico') 
      
    # Función que muestra la grafica 
    plt.show() 
    return True

"""
Algoritmo Bubble Sort
Es un algoritmo de ordenamiento que permite hacer swap o intercambio entre los elementos adyacentes de un arreglo.
"""
def bubbleSort(arreglo):
    #Calcula el tamaño del arreglo
    n = len(arreglo)
 
    # Recorre el arreglo
    for i in range(n):
 
        #Recorre el arreglo de 0 a n-i-1, ya que los ultimos i elementos ya estan en la ubicacion correcta.
        for j in range(0, n-i-1):
 
            # Si el elemento que encuentra es mayor, intercambia la ubicación con el al lado.
            if arreglo[j][1] > arreglo[j+1][1] :
                #Intercambia los valores de y o el diametro.
                arreglo[j][1], arreglo[j+1][1] = arreglo[j+1][1], arreglo[j][1]
                #Intercambia los labels
                arreglo[j][0], arreglo[j+1][0] = arreglo[j+1][0], arreglo[j][0]

                #Imprime el arreglo si hace un cambio
                print("Cambia a")
                print(arreglo)
'''
Funcion que verifica si los argumentos tienen la estructura de un array, es decir, 
tiene los corchetes.
'''
def checkInput(input):
    s="[" in input
    y="]" in input
    if (not s and not y):
        print(input+' no tiene [ y ]')
    elif (not s):
        print(input+' no tiene [')
    elif (not y):
        print(input+' no tiene ]')
    return (s and y)

if __name__ == '__main__': 

    #Chequea que tenga el largo adecuado de argumentos.
    if len(sys.argv) == 2:
        check1=checkInput(sys.argv[1])
        if (not check1):
            exit()

        #Limpiando input de diametros
        diam_input=sys.argv[1].replace("[","")
        diam_input=diam_input.replace("]","")
        diam_input=diam_input.split(",")

        array=[]
        try:
            for i in range(len(diam_input)):
                array.append([str(i+1), int(diam_input[i])])

            print("Originalmente se tiene:")
            print(array)

            #Llama a las funciones
            bubbleSort(array)
            graficar(array)
        except:
            print("Ingreso un valor no numerico en el arreglo del eje y")
    elif len(sys.argv) != 3 and len(sys.argv) != 2:
        print("\nLa cantidad de argumentos ingresada no es correcta \nPara este reto se aceptan dos tipos de formato:")
        print(" 1. Solo indicando los diametros ( python3 reto1.py [array con diametros (eje y)] ) ")
        print(" 2. Indicando los diametros y los labels ( python3 reto1.py [array con diametros (eje y)] [array con labels de eje x] ) \n")
        print(" Ejemplo formato 1: python3 reto1.py [1,5,2] \n Ejemplo formato 2: python3 reto1.py [1,5,2] [1,2,3]")
        print("\n Obs: Dentro del array no se permiten espacios debido a que aumentaria la cantidad de argumentos recibidos")
    else:
        check1=checkInput(sys.argv[1])
        if (not check1):
            exit()

        #Limpiando input de diametros
        diam_input=sys.argv[1].replace("[","")
        diam_input=diam_input.replace("]","")
        diam_input=diam_input.split(",")

        check2=checkInput(sys.argv[2])
        if (not check2):
            exit()

        #Limpiando input de labels
        labels_input=sys.argv[2].replace("[","")
        labels_input=labels_input.replace("]","")
        labels_input=labels_input.split(",")

        #Verifica que tenga el mismo largo.
        if len(diam_input)!=len(labels_input):
            print("El largo de los arreglos no coincide")
        else:
            array=[]
            try:
                for i in range(len(diam_input)):
                    array.append([labels_input[i], int(diam_input[i])])

                print("Originalmente se tiene:")
                print(array)

                #Llama a las funciones
                bubbleSort(array)
                graficar(array)
            except:
                print("Ingreso un valor no numerico en el arreglo del eje y")